#!/usr/bin/node
"use strict"

const fs = require("fs-extra");
require("colors");
const { readdirSync, statSync } = require('fs');
const { join, resolve } = require('path');
const moment = require("moment");
const jsome = require("jsome");
const yaml = require("js-yaml");
const sprintf = require('sprintf-js').sprintf;

const { prompt } = require('enquirer');

const question_path = join(__dirname, "/data/", "test.yml");

let file_data = fs.readFile(question_path, "utf8").then(yaml.safeLoad);

file_data.then( function(data) {

let question_options=[];

let index=0;
for( let q of data.questions) {
	index++;
	const question = {};
	question_options.push({
		type: 'Select',
		name: String(index),
		message: q.question,
		choices: q.alternatives
	});
	}

const befor_time = moment();

prompt(question_options).then((answer) => {
  let points = 0;
  const after_time = moment();
  const diff_time=after_time-befor_time; 
  
  for(let i=0; i < Object.keys(answer).length;i++){
	  if(answer[String(i+1)] === String(data.questions[i].answer)) {
	     points++;
      }   
  }
  console.log("\nPoäng: ".bold+(points +"/"+Object.keys(answer).length).green);
  
  let min=Math.round(diff_time/1000/60);
  let sek=Math.round(diff_time/1000)%60;
  console.log("Tid:   ".bold+sprintf("%d:%02d", min, sek).green);
}
);

});

